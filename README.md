# Treelogism-server

## Introduction :

Treelogism est une application qui a pour but de réduire le nombre de déchets sur la planète.

## Fonctionnement :

Le serveur de Treelogism est développer sous NodeJS avec le framework Express.
Vous ne pourrez pas faire tourner ce projet si vous n'avez pas la base de données mysql.
Sur la racine, il y a le fichier `connexion.js` qui fait la connexion a la base de données,
n'oubliez pas de le modifier si vous avez une base de données.
Ensuite le projet se compose d'un dossier `configs` qui comprend le JWT_SECRET qui permet l'authentification,
n'oubliez pas de le remplir pour pouvoir signer vos token de connexion.
Après, il y a le dossier `controllers` comprend toutes les requêtes SQL spécifiquent à une table avec différentes fonctions.
Ensuite nous avons le dossier `routes`, où il y a tout les end points de l'API. Puis pour finir, le dossier `uploads`
 qui comprend tous les fichiers générés comme les posts par exemple.

## Pré-requis :

Pour utiliser cet API, vous avez besoin de :
* `nodeJS`.
* `npm`.
* `nodemon`

## Utilisation :

* `git clone https://gitlab.com/suleydogan70/treelogism-server.git`
* `cd treelogism-server`
* `npm install`
* `npm start` ou changez les scripts dans le fichier package.json

## Actions disponibles :

*Voir les endpoints*

* Se connecter
* S'inscrire
* Ajouter un post
* Liker un post
* Supprimer un post
* Pin la position actuelle
* Ajouter un ami
* Supprimer un ami
* Visualiser une carte où les autres ont pin
* Voir une notification
* Supprimer une notification
* Voir le profil d'un utilisateur
* Modifier son profil
