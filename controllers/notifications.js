const connexion = require('../connexion')
/*
  === TYPES DE NOTIFICATIONS ===
  1 - Demande d'invitation
  2 - Invitation acceptée
  3 - Post liké
  4 - Nouvelle publication
  @params user_id : int, x : string, notification_time : timestamp
*/
const notificationsCtrl = {
  async getNotifications(user_id) {
    const queryString = `select
    n.id_notification,
    timestampdiff(second, n.notification_time, now()) as notification_time,
    n.user_id,
    n.x,
    nt.*
    from notifications n
    inner join notification_types nt on nt.notification_type_id = n.notification_type_id
    where n.user_id = ?`
    return await connexion.query(queryString, [user_id])
  },
  async newNotification(user_id, type, x, date) {
    const queryString = `insert into notifications
    (user_id, notification_type_id, x, notification_time)
    values (?, ?, ?, ?)`
    return await connexion.query(queryString, [user_id, type, x, date])
  },
  async dismissNotification(notifId) {
    const queryString = `delete
    from notifications
    where id_notification = ?`
    return await connexion.query(queryString, [notifId])
  }
}

module.exports = notificationsCtrl
