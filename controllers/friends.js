const connexion = require('../connexion')

const friendsCtrl = {
  async getUserFriendList(user_id) {
    const queryString = `select
    u.user_id,
    u.user_lastname,
    u.user_firstname,
    u.user_avatar,
    u.user_leafs,
    t.title_name,
    f.friendship_state,
    f.sender
    from users u, titles t, friends f
    where f.user_id = ?
    and f.friend_id = u.user_id
    and u.title_id = t.title_id`
    return await connexion.query(queryString, [user_id])
  },
  async getUserFriendsSuggestion(user_id) {
    const queryString = `select
    u.user_id,
    u.user_lastname,
    u.user_firstname,
    u.user_avatar,
    u.user_leafs,
    t.title_name
    from users u, titles t
    where u.user_id not in (select f.friend_id from friends f where f.user_id = ?)
    and u.user_id != ?
    and u.title_id = t.title_id
    group by u.user_id
    limit 5`
    return await connexion.query(queryString, [user_id, user_id, user_id])
  },
  async getUserFriendsIds(user_id) {
    const queryString = `select
    f.friend_id
    from friends f
    where f.user_id = ?
    and f.friendship_state = 1`
    return await connexion.query(queryString, [user_id])
  },
  async deleteFriend(user_id, friend_id) {
    let queryString = `delete
    from friends
    where friends.user_id = ?
    and friends.friend_id = ?`
    return await connexion.query(queryString, [user_id, friend_id])
  },
  async addFriend(user_id, friend_id, sender) {
    let queryString = `insert into friends
    (user_id, friend_id, friendship_state, sender)
    values (?, ?, ?, ?)`
    return await connexion.query(queryString, [user_id, friend_id, 0, sender])
  },
  async validateFriend(user_id, friend_id) {
    let queryString = `update friends
    set friendship_state = 1
    where user_id = ?
    and friend_id = ?`
    return await connexion.query(queryString, [user_id, friend_id])
  }
}

module.exports = friendsCtrl
