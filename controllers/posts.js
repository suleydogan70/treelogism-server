const connexion = require('../connexion')

const postsCtrl = {
  async getAllPosts() {
    const queryString = `select
    p.post_id,
    timestampdiff(second, p.post_publication_datetime, now()) as post_date,
    p.post_image,
    u.user_id,
    u.user_lastname,
    u.user_firstname,
    u.user_avatar,
    t.title_name,
    (select count(l.post_id) from liked_posts l where l.post_id = p.post_id) as likes
    from users u, titles t, posts p
    where p.user_id = u.user_id
    and u.title_id = t.title_id
    order by p.post_publication_datetime DESC`
    return await connexion.query(queryString)
  },
  async getThePostUserId(postId) {
    const queryString = `select
    p.user_id
    from posts p
    where p.post_id = ?`
    return await connexion.query(queryString, postId)
  },
  async getUserPosts(user_id) {
    const queryString = `select
    p.post_id,
    timestampdiff(second, p.post_publication_datetime, now()) as post_date,
    p.post_image,
    u.user_id,
    u.user_lastname,
    u.user_firstname,
    u.user_avatar,
    t.title_name,
    (select count(l.post_id) from liked_posts l where l.post_id = p.post_id) as likes
    from users u, titles t, posts p
    where p.user_id = u.user_id
    and p.user_id = ?
    and u.title_id = t.title_id
    order by p.post_publication_datetime DESC`
    return await connexion.query(queryString, [user_id])
  },
  async getLikedPosts(user_id) {
    const queryString = `select
    post_id
    from liked_posts
    where user_id = ?`
    return await connexion.query(queryString, [user_id])
  },
  async likePost(user_id, post_id) {
    const queryString = `insert into
    liked_posts (user_id, post_id)
    values (?, ?)`
    return await connexion.query(queryString, [user_id, post_id])
  },
  async dislikePost(user_id, post_id) {
    const queryString = `delete
    from liked_posts
    where user_id = ?
    and post_id = ?`
    return await connexion.query(queryString, [user_id, post_id])
  },
  async addPost(date, img, user_id) {
    const queryString = `insert into
    posts (post_publication_datetime, post_image, user_id)
    values (?, ?, ?)`
    return await connexion.query(queryString, [date, img, user_id])
  },
  async getPostImage(post_id) {
    const queryString = `select
    p.post_image
    from posts p
    where p.post_id = ?`
    return await connexion.query(queryString, [post_id])
  },
  async deletePost(post_id) {
    const queryString = `delete
    from posts
    where posts.post_id = ?`
    return await connexion.query(queryString, [post_id])
  },
  async deleteLikes(post_id) {
    const queryString = `delete
    from liked_posts
    where liked_posts.post_id = ?`
    return await connexion.query(queryString, [post_id])
  }
}

module.exports = postsCtrl
