const connexion = require('../connexion')

const mapsCtrl = {
  async pinPosition(latitude, longitude, user) {
    const queryString = `insert into
    pins (latitude, longitude, user)
    values (?, ?, ?)`
    return await connexion.query(queryString, [latitude, longitude, user])
  },
  async getPins() {
    const queryString = `select
    p.latitude,
    p.longitude,
    CONCAT(u.user_firstname, ' ', u.user_lastname) as fullname,
    u.user_leafs,
    u.user_avatar,
    t.title_name
    from pins p, users u, titles t
    where p.user = u.user_id
    and u.title_id = t.title_id`
    return await connexion.query(queryString)
  }
}

module.exports = mapsCtrl
