const connexion = require('../connexion')

const usersCtrl = {
  async checkUserExist(mail) {
    const queryString = `select
    u.user_id,
    u.user_psw
    from users u
    where u.user_mail = ?`
    return await connexion.query(queryString, [mail])
  },
  async addUser(mail, pass, firstname, lastname) {
    const queryString = `insert into users(
      user_mail,
      user_psw,
      user_firstname,
      user_lastname
    )
    values (?, ?, ?, ?)`

    return await connexion.query(queryString, [mail, pass, firstname, lastname])
  },
  async findUserById(user_id) {
    const queryString = `select
    u.user_id,
    u.user_mail,
    u.user_firstname,
    u.user_lastname,
    u.user_avatar,
    u.user_description,
    u.admin,
    u.user_leafs,
    u.title_id,
    t.title_name,
    t.title_max_leafs
    from users u, titles t
    where u.user_id = ?
    and u.title_id = t.title_id`
    return await connexion.query(queryString, [user_id])
  },
  async updateLeafs(user_id, leafs) {
    const queryString = `update users u
    set u.user_leafs = ?
    where u.user_id = ?`
    return await connexion.query(queryString, [leafs, user_id])
  },
  async updateTitle(user_id, title) {
    const queryString = `update users u
    set u.title_id = ?
    where u.user_id = ?`
    return await connexion.query(queryString, [title, user_id])
  },
  async getAllUsers() {
    const queryString = `select
    u.user_id,
    concat(u.user_firstname, ' ', u.user_lastname) as fullname
    from users u`
    return await connexion.query(queryString)
  },
  async updateUser(firstname, lastname, mail, desc, id) {
    const queryString = `update users
    set user_firstname = ?, user_lastname = ?, user_mail = ?, user_description = ?
    where user_id = ?`
    return await connexion.query(queryString, [firstname, lastname, mail, desc, id])
  }
}

module.exports = usersCtrl
