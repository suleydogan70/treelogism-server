const express = require('express')
const friendsRoute = express.Router()
const JWT = require('jsonwebtoken')
const { JWT_SECRET } = require('../configs')
const connexion = require('../connexion')
const friendsCtrl = require('../controllers/friends')
const notificationsCtrl = require('../controllers/notifications')

verifyToken = (req, res, next) => {
  const { authorization } = req.headers
  if (authorization === undefined) {
    res.status(403)
  } else {
    const bearer = authorization.split(' ')
    req.token = bearer[1]
  }
  next()
}

friendsRoute.post('/', verifyToken, (req, res) => {
  const { token } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      const response = await friendsCtrl.getUserFriendList(authData.sub)
      const response2 = await friendsCtrl.getUserFriendsSuggestion(authData.sub)
      res.json({ friends: response, suggestions: response2 })
    } catch (error) {
      res.json({ err: `An error has occured while fetching your friend list` })
    }
  })
})

friendsRoute.post('/delete_friend', verifyToken, (req, res) => {
  const { token, body: {friendId} } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await friendsCtrl.deleteFriend(authData.sub, friendId)
      await friendsCtrl.deleteFriend(friendId, authData.sub)
      const response = await friendsCtrl.getUserFriendList(authData.sub)
      const response2 = await friendsCtrl.getUserFriendsSuggestion(authData.sub)
      res.json({ friends: response, suggestions: response2 })
    } catch (error) {
      res.json({ err: `An error has occured while trying to delete this friend` })
    }
  })
})

friendsRoute.post('/add_friend', verifyToken, (req, res) => {
  const { token, body: { friendId, user } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await friendsCtrl.addFriend(authData.sub, friendId, 1)
      await friendsCtrl.addFriend(friendId, authData.sub, 0)
      await notificationsCtrl.newNotification(friendId, 1, user, new Date().toLocaleString())
      const response = await friendsCtrl.getUserFriendList(authData.sub)
      const response2 = await friendsCtrl.getUserFriendsSuggestion(authData.sub)
      res.json({ friends: response, suggestions: response2 })
    } catch (error) {
      console.log(error)
      res.json({ err: `An error has occured while trying to add this friend` })
    }
  })
})

friendsRoute.post('/validate_friend', verifyToken, (req, res) => {
  const { token, body: { friendId, user } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await friendsCtrl.validateFriend(authData.sub, friendId)
      await friendsCtrl.validateFriend(friendId, authData.sub)
      await notificationsCtrl.newNotification(friendId, 2, user, new Date().toLocaleString())
      const response = await friendsCtrl.getUserFriendList(authData.sub)
      const response2 = await friendsCtrl.getUserFriendsSuggestion(authData.sub)
      res.json({ friends: response, suggestions: response2 })
    } catch (error) {
      res.json({ err: `An error has occured while trying to accept this friend` })
    }
  })
})

friendsRoute.post('/user_friends', async (req, res) => {
  try {
    const response = await friendsCtrl.getUserFriendList(req.body.userId)
    res.json({ friends: response })
  } catch (error) {
    res.json({ err: `An error has occured while fetching your friend list` })
  }
})

module.exports = friendsRoute
