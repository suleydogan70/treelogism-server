const express = require('express')
const uploadsRoute = express.Router()
const fs = require('fs')

uploadsRoute.get('/:img', (req, res) => {
  const { img } = req.params
  const file = fs.readFileSync(`uploads/${img}`)
  res.send(file)
})

module.exports = uploadsRoute
