const express = require('express')
const postsRoute = express.Router()
const JWT = require('jsonwebtoken')
const fs = require('fs')
const path = require('path')
const { JWT_SECRET } = require('../configs')
const connexion = require('../connexion')
const postsCtrl = require('../controllers/posts')
const friendsCtrl = require('../controllers/friends')
const notificationsCtrl = require('../controllers/notifications')
const usersCtrl = require('../controllers/users')

verifyToken = (req, res, next) => {
  const { authorization } = req.headers
  if (authorization === undefined) {
    res.status(403)
  } else {
    const bearer = authorization.split(' ')
    req.token = bearer[1]
  }
  next()
}

postsRoute.post('/', verifyToken, (req, res) => {
  const { token } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      const response = await postsCtrl.getAllPosts()
      const response2 = await postsCtrl.getLikedPosts(authData.sub)
      res.json({ posts: response, liked_posts: response2 })
    } catch (error) {
      console.log(error)
      res.json({ err: 'Cannot get posts' })
    }
  })
})

postsRoute.post('/user_posts', async (req, res) => {
  try {
    const response = await postsCtrl.getUserPosts(req.body.userId)
    res.json({ posts: response })
  } catch (error) {
    console.log(error)
    res.json({ err: 'An error has occured while fetching posts' })
  }
})

postsRoute.post('/like_post', verifyToken, (req, res) => {
  const { token, body: { postId, user } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await postsCtrl.likePost(authData.sub, postId)
      const userId = await postsCtrl.getThePostUserId(postId)
      await notificationsCtrl.newNotification(userId[0].user_id, 3, user, new Date().toLocaleString())
      const response = await postsCtrl.getAllPosts()
      const response2 = await postsCtrl.getLikedPosts(authData.sub)
      res.json({ posts: response, liked_posts: response2 })
    } catch (error) {
      console.log(error)
      res.json({ err: 'The post can\'t be liked' })
    }
  })
})

postsRoute.post('/dislike_post', verifyToken, (req, res) => {
  const { token, body: { postId } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await postsCtrl.dislikePost(authData.sub, postId)
      const response = await postsCtrl.getAllPosts()
      const response2 = await postsCtrl.getLikedPosts(authData.sub)
      res.json({ posts: response, liked_posts: response2 })
    } catch (error) {
      console.log(error)
      res.json({ err: 'The post can\'t be disliked' })
    }
  })
})

postsRoute.post('/delete_post', verifyToken, (req, res) => {
  const { token, body: { postId } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      const file = await postsCtrl.getPostImage(postId)
      fs.unlink(`uploads/${file[0].post_image}`, err => {
        if (err) {
          throw err
        }
      })
      await postsCtrl.deleteLikes(postId)
      await postsCtrl.deletePost(postId)
      const response = await postsCtrl.getAllPosts()
      const response2 = await postsCtrl.getLikedPosts(authData.sub)
      res.json({ posts: response, liked_posts: response2 })
    } catch (error) {
      console.log(error)
      res.json({ err: 'An error has occured' })
    }
  })
})

postsRoute.post('/newPostTxt', verifyToken, (req, res) => {
  const { token, body: { img, user } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err || !img) {
      return res.sendStatus(403)
    }
    const filename = `out${new Date().getTime()}.png`
    fs.writeFile('uploads/' + filename, img, 'base64', function(err) {
      if (err) {
        return res.json({ err: 'File upload failed' })
      }
    })
    try {
      await postsCtrl.addPost(new Date().toLocaleString(), filename, authData.sub)
      const theUser = await usersCtrl.findUserById(authData.sub)
      const max_leafs = theUser[0].title_max_leafs
      const user_leafs = theUser[0].user_leafs
      const friends = await friendsCtrl.getUserFriendsIds(authData.sub)
      friends.map(async friend => {
        await notificationsCtrl.newNotification(friend.friend_id, 4, user, new Date().toLocaleString())
      })
      await usersCtrl.updateLeafs(authData.sub, user_leafs + 75)
      if ((user_leafs + 75) > max_leafs) {
        await usersCtrl.updateTitle(authData.sub, theUser[0].title_id + 1)
      }
      const response = await postsCtrl.getAllPosts()
      try {
        const response2 = await postsCtrl.getLikedPosts(authData.sub)
        res.json({ posts: response, liked_posts: response2 })
      } catch (error) {
        console.log(error)
        res.json({ err: 'An error has occured while getting liked posts' })
      }
    } catch (error) {
      console.log(error)
      res.json({ err: 'An error has occured while fetching posts' })
    }
  })
})

postsRoute.post('/newPostFile', verifyToken, (req, res) => {
  const { token, file } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err || !file) {
      return res.sendStatus(403)
    }
    try {
      await postsCtrl.addPost(new Date().toLocaleString(), file.filename, authData.sub)
      const user = await usersCtrl.findUserById(authData.sub)
      const username = `${user[0].user_firstname} ${user[0].user_lastname}`
      const max_leafs = user[0].title_max_leafs
      const user_leafs = user[0].user_leafs
      const friends = await friendsCtrl.getUserFriendsIds(authData.sub)
      friends.map(async friend => {
        await notificationsCtrl.newNotification(friend.friend_id, 4, username, new Date().toLocaleString())
      })
      await usersCtrl.updateLeafs(authData.sub, user_leafs + 75)
      if ((user_leafs + 75) > max_leafs) {
        await usersCtrl.updateTitle(authData.sub, user[0].title_id + 1)
      }
      const response = await postsCtrl.getAllPosts()
      try {
        const response2 = await postsCtrl.getLikedPosts(authData.sub)
        res.json({ posts: response, liked_posts: response2 })
      } catch (error) {
        console.log(error)
        res.json({ err: 'An error has occured while getting liked posts' })
      }
    } catch (error) {
      console.log(error)
      res.json({ err: 'An error has occured while fetching posts' })
    }
  })
})

module.exports = postsRoute
