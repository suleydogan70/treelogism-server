const express = require('express')
const mapsRoute = express.Router()
const JWT = require('jsonwebtoken')
const { JWT_SECRET } = require('../configs')
const connexion = require('../connexion')
const mapsCtrl = require('../controllers/maps')

const formatNotifs = notifications => {
  const notifs = []
  notifications.map(item => {
    notifs.push({
      id: item.id_notification,
      user: item.user_id,
      type: item.notification_type_id,
      text: item.notification_type_text.replace('X', item.x),
      time: item.notification_time
    })
  })
  return notifs
}

verifyToken = (req, res, next) => {
  const { authorization } = req.headers
  if (authorization === undefined) {
    res.status(403)
  } else {
    const bearer = authorization.split(' ')
    req.token = bearer[1]
  }
  next()
}

mapsRoute.post('/pin', verifyToken, (req, res) => {
  const { token, body: { latitude, longitude } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await mapsCtrl.pinPosition(latitude, longitude, authData.sub)
    } catch (error) {
      console.log(error)
    }
  })
})

mapsRoute.get('/', async (req, res) => {
  try {
    const pins = await mapsCtrl.getPins()
    res.json({ pins })
  } catch (error) {
    console.log(error)
  }
})

module.exports = mapsRoute
