const express = require('express')
const JWT = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const userRoute = express.Router()
const connexion = require('../connexion')
const usersCtrl = require('../controllers/users')
const { JWT_SECRET } = require('../configs')
const passwordRegex = new RegExp(`^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})`)

signToken = userId => {
  return JWT.sign({
    iss: 'Splitziz',
    sub: userId
  }, JWT_SECRET)
}

verifyToken = (req, res, next) => {
  const { authorization } = req.headers
  if (authorization === undefined) {
    res.status(403)
  } else {
    const bearer = authorization.split(' ')
    req.token = bearer[1]
  }
  next()
}

userRoute.post('/connexion', (req, res) => {
  const { mail, pass } = req.body
  // Terminate the execution if one field is empty
  if (mail === undefined || mail === null)
    return res.json({ err: `The email field is empty` })
  if (pass === undefined || pass === null)
    return res.json({ err: `The password field is empty` })
  // check if the mail is used, to stop the function if it's not
  usersCtrl.checkUserExist(mail)
  .then(
    (response) => {
      // if we can't get the User's datas, then the account doesn't exist
      if(response[0] === undefined)
        return res.json({ err: `This account doesn't exist` })
      bcrypt.compare(pass, response[0].user_psw, function(err, result) {
        if(!result) {
          return res.status(200).json({ err: `You typed a wrong password` })
        }
        usersCtrl.findUserById(response[0].user_id)
        .then(
          (data) => {
            const token = signToken(data[0].user_id)
            res.status(200).json({ token, user: data[0] })
          },
          (error) => {
            res.status(200).json({ err: `An error has occured while trying to get the User's datas` })
          }
        )
      })
    },
    (error) => {
      res.status(200).json({ err: `An error has occured while trying to check if the User exist` })
    }
  )
})

userRoute.post('/inscription', (req, res) => {
  const { mail, pass, firstname, lastname } = req.body
  // Terminate the execution if one field is empty
  if (mail === undefined || mail === null)
    return res.json({ err: `The email field is empty` })
  if (pass === undefined || pass === null)
    return res.json({ err: `The password field is empty` })
  if (firstname === undefined || firstname === null)
    return res.json({ err: `The firstname field is empty` })
  if (lastname === undefined || lastname === null)
    return res.json({ err: `The lastname field is empty` })
  // If the user mail already exist in the db
  usersCtrl.checkUserExist(mail).then(
    async (response) => {
      if(response[0] !== undefined)
        return res.json({ err: `This email is already used` })
      // If the password field doesn't respect the RegExp, return error
      if(!passwordRegex.test(pass))
        return res.json({
          err: `The password must contain more than 6 characters, 1 special character, 1 lowercase character, 1 uppercase character and 1 numeric character`
        })
      const salt = await bcrypt.genSalt(10)
      const hashedPassword = await bcrypt.hash(pass, salt)
      usersCtrl.addUser(mail, hashedPassword, firstname, lastname)
      .then(
        (response) => {
          // If the User is not created, send error
          if(response.insertId === undefined || response.insertId === null)
            return res.json({ err: `An error has occured` })
          // get the added user's data and send them with connexion token
          usersCtrl.findUserById(response.insertId)
          .then(
            (response) => {
              const token = signToken(response[0].user_id)
              res.status(200).json({ token, user: response[0] })
            },
            (error) => {
              res.status(200).json({ err: `An error has occured while trying to find the User` })
            }
          )
        },
        (error) => {
          res.status(200).json({ err: `An error has occured while trying to save the User` })
        }
      )
    },
    (error) => {
      res.status(200).json({ err: `An error has occured while trying to check if the User exist` })
    }
  )
})

userRoute.post('/getUser', verifyToken, (req, res) => {
  const { token } = req
  JWT.verify(token, JWT_SECRET, (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    usersCtrl.findUserById(authData.sub)
      .then(
        (response) => {
          res.json({ user: response[0], token })
        },
        (error) => {
          res.json({ err: `This user doesn't exist` })
        }
      )
  })
})

userRoute.get('/user/:id', (req, res) => {
  usersCtrl.findUserById(req.params.id)
    .then(
      (response) => {
        if (response.length === 0) {
          return res.json({ err: `This user doesn't exist` })
        }
        res.json({ user: response[0] })
      },
      (error) => {
        res.json({ err: `This user doesn't exist` })
      }
    )
})

userRoute.get('/allUsers', async (req, res) => {
  try {
    const response = await usersCtrl.getAllUsers()
    res.json({ users: response })
  } catch (error) {
    console.log(error)
  }
})

userRoute.post('/update', verifyToken, (req, res) => {
  const { token, body: { username, usermail, userdesc } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await usersCtrl.updateUser(username.split(' ')[0], username.split(' ')[1], usermail, userdesc, authData.sub)
      const response = await usersCtrl.findUserById(authData.sub)
      res.json({ user: response[0] })
    } catch (error) {
      res.json({ err: 'cannot update this user' })
    }

  })
})

module.exports = userRoute
