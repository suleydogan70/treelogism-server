const express = require('express')
const notificationsRoute = express.Router()
const JWT = require('jsonwebtoken')
const { JWT_SECRET } = require('../configs')
const connexion = require('../connexion')
const notificationsCtrl = require('../controllers/notifications')

const formatNotifs = notifications => {
  const notifs = []
  notifications.map(item => {
    notifs.push({
      id: item.id_notification,
      user: item.user_id,
      type: item.notification_type_id,
      text: item.notification_type_text.replace('X', item.x),
      time: item.notification_time
    })
  })
  return notifs
}

verifyToken = (req, res, next) => {
  const { authorization } = req.headers
  if (authorization === undefined) {
    res.status(403)
  } else {
    const bearer = authorization.split(' ')
    req.token = bearer[1]
  }
  next()
}

notificationsRoute.post('/', verifyToken, (req, res) => {
  const { token } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      const response = await notificationsCtrl.getNotifications(authData.sub)
      res.json({ notifications: formatNotifs(response) })
    } catch (error) {
      res.json({ err: `An error has occured while fetching your notifications` })
    }
  })
})

notificationsRoute.post('/dismiss_notif', verifyToken, (req, res) => {
  const { token, body: { notifId } } = req
  JWT.verify(token, JWT_SECRET, async (err, authData) => {
    if (err) {
      return res.sendStatus(403)
    }
    try {
      await notificationsCtrl.dismissNotification(notifId)
      const response = await notificationsCtrl.getNotifications(authData.sub)
      res.json({ notifications: formatNotifs(response) })
    } catch (error) {
      res.json({ err: `An error has occured while fetching your notifications` })
    }
  })
})

module.exports = notificationsRoute
