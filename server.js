const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const multer = require('multer')
const path = require('path')
const app = express()
const userRoutes = require('./routes/user')
const postsRoutes = require('./routes/posts')
const friendsRoutes = require('./routes/friends')
const uploadsRoute = require('./routes/uploads')
const notificationsRoute = require('./routes/notifications')
const mapsRoute = require('./routes/maps')
const maxSize = 4 * 1000 * 1000;

const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})
const upload = multer({
 limits:{
  fileSize: maxSize
 },
 fileFilter: function (req, file, cb) {
   const filetypes = /jpeg|jpg|png/
   const mimetype = filetypes.test(file.mimetype)
   const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
   if (mimetype && extname && file.originalname === escape(file.originalname)) {
     return cb(null, true)
   }
   cb("Error: File upload not valid")
 },
 dest: 'uploads/'
})

/* --- MiddleWares --- */
app.use(bodyParser.urlencoded({extended: true}))
app.use(upload.single('file'))
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      imgSrc: ['*'],
      upgradeInsecureRequests: true,
    },
  })
)
app.use(helmet.frameguard({ action: 'deny' }))
app.use(helmet.noSniff())
app.use(
  helmet.hsts({ maxAge: 31536000, includeSubDomains: true, preload: true })
)
app.use(helmet.ieNoOpen())
app.use(helmet.referrerPolicy({ policy: 'no-referrer' }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*")
    res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE')
    res.header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
    next()
})
app.use('/users', userRoutes)
app.use('/posts', postsRoutes)
app.use('/friends', friendsRoutes)
app.use('/uploads', uploadsRoute)
app.use('/notifications', notificationsRoute)
app.use('/maps', mapsRoute)
/* --- MiddleWares --- */

app.listen(1234)
